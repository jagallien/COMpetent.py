import os
import re
import subprocess


def pathfinder(arg):
    #finds path, splits each directory within path into different list items
    path = (os.environ['PATH']).split(';')
    num = -1

    for i in path:
        putty = re.search('putty', i, re.IGNORECASE)
        securecrt = re.search('vandyke', i, re.IGNORECASE)
        num += 1

        if securecrt:
            os.system('SecureCRT /SERIAL ' + arg + ' /BAUD 9600 /DATA 8 /STOP 0')
            break
        elif putty:
            os.system('putty -serial ' + arg)
            break
        else:
            continue

def find_port():

    #find COM port, and the number that comes after it.
    #the combination of these two is then stored as the 'arg' variable

    result = subprocess.Popen('MODE', shell=True,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    output,error = result.communicate()
    output = str(output)


    com_port = output.rfind('COM')
    arg = (output[com_port:(com_port+4)])
    return arg



if os.name == 'nt':

    if 'COM' in find_port():
        pathfinder(find_port())
    elif 'COM' not in find_port(): 
        print('\nNo serial cable connected.\n')
    else:
        print('Error: You shouldn\'t be seeing this message.\n')
else:
    print('\nWhy are you using this script?\nYour OS uses /dev/ttyUSB0, schmuck.')
