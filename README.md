# COMpetent.py
#### Automatically finds the COM port that your serial cable is plugged into, and starts a PuTTY session with it.

##### Requirements:
  - [Python 3.6]
  - NOTE: you must check the "add path variable" box during installation!
  - [PuTTY]
  - Windows

[Python 3.6]: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe
[PuTTY]: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html


Contact: feel free to reach out to me at j.a.gallien@gmail.com